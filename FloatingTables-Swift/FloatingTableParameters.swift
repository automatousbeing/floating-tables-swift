//
//  FloatingTableParameters.swift
//  FloatingTables-Swift
//
//  Created by Stephan Hennion on 7/31/15.
//  Copyright (c) 2015 Stephan Hennion. All rights reserved.
//

import Foundation
import UIKit

// This class represents the objects passed into the FloatingTableInitializer, and the values that don't change
// during execution
class FloatingTableParameters {
    let viewFrame : CGRect
    let columnWidths : [Int]
    var columnTotals : [Int]
    var columnSum : Int
    var smallestColumn : Int
    let rowHeights : [Int]
    var rowTotals : [Int]
    var rowSum : Int
    var smallestRow : Int
    let thresholdPercentage : CGFloat
    let cellBufferX : CGFloat
    let cellBufferY : CGFloat
    
    init(viewFrame: CGRect, columnWidths : [Int], rowHeights : [Int], thresholdPercentage : CGFloat, cellBufferX : CGFloat, cellBufferY : CGFloat) {
        self.viewFrame = viewFrame
        self.columnWidths = columnWidths
        self.rowHeights = rowHeights
        self.thresholdPercentage = thresholdPercentage
        self.cellBufferX = cellBufferX
        self.cellBufferY = cellBufferY
        self.columnTotals = []
        self.rowTotals = []
        self.columnSum = 0
        self.rowSum = 0
        self.smallestColumn = 0
        self.smallestRow = 0
        self.columnTotals = calculateArrayTotals(columnWidths)
        self.rowTotals = calculateArrayTotals(rowHeights)
        self.columnSum = calculateArraySums(columnWidths)
        self.rowSum = calculateArraySums(rowHeights)
        self.smallestColumn = calculateArraySmallest(columnWidths)
        self.smallestRow = calculateArraySmallest(rowHeights)
    }
}

// This extension exists just to prepare the parameters that are calculated via initial values passed in to the constructor
extension FloatingTableParameters {
    func calculateArrayTotals(sizes : [Int]) -> [Int] {
        var totals = [Int]()
        var runningTotal = 0
        
        for size in sizes {
            totals.append(runningTotal)
            runningTotal += size
        }
        
        return totals
    }
    
    func calculateArraySums(sizes : [Int]) -> Int {
        var runningTotal = 0
        
        for size in sizes {
            runningTotal += size
        }
        
        return runningTotal
    }
    
    func calculateArraySmallest(sizes: [Int]) -> Int {
        var curSmallest = sizes[0]
        
        for size in sizes {
            if size < curSmallest {
                curSmallest = size
            }
        }
        
        return curSmallest
    }
}