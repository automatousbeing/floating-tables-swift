//
//   CellDatasource.swift
//  FloatingTables-Swift
//
//  Created by Stephan Hennion on 7/31/15.
//  Copyright (c) 2015 Stephan Hennion. All rights reserved.
//

import Foundation

protocol CellDatasourceDelegate {
    func cellResultsReturn(results: [[String]])
}

class CellDatasource : NSObject, NSURLConnectionDelegate {
    // This waits until the first data append
    lazy var data = NSMutableData()
    
    var width = 1
    var height = 1
    
    var delegate : CellDatasourceDelegate?
    
    func setWidth(width : Int, height : Int) {
        self.width = width
        self.height = height
    }
    
    func startConnection() {
        // A little security through obfuscation, so my droplet IP isn't sitting in a public bitbucket repo
        let decodedData = NSData(base64EncodedString: "aHR0cDovLzEwNC4yMzYuMjEyLjY0OjUwMzg1L2dldENlbGxEYXRhPw==", options: NSDataBase64DecodingOptions(0))
        let urlBase = NSString(data: decodedData!, encoding: NSUTF8StringEncoding)
        let widthString = String(format: "width=%d&", width)
        let heightString = String(format: "height=%d", height)
        let totalUrl = (urlBase! as String) + widthString + heightString
        var url: NSURL = NSURL(string: totalUrl)!
        var request: NSURLRequest = NSURLRequest(URL: url)
        var connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: false)!
        connection.start()
    }
    
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!) {
        self.data.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!) {
        var err: NSError?
        if let jsonResult: NSArray = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: &err) as? [[String]]
        {
            if let delegate = self.delegate
            {
                delegate.cellResultsReturn(jsonResult as! [[String]])
                data.setData(NSData())
            }
            else
            {
                println("Delegate undefined")
            }
        } else {
            println(err)
        }
    }
}