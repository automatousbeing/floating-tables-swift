//
//  ViewController.swift
//  FloatingTables-Swift
//
//  Created by Stephan Hennion on 7/31/15.
//  Copyright (c) 2015 Stephan Hennion. All rights reserved.
//

import UIKit

class ViewController: UIViewController, CellDatasourceDelegate {
    var cellDatasource : CellDatasource?
    var tableView : FloatingTableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Change these values to adjust the table size!
        let width = 150
        let height = 120
        
        // Setup the CellDatasource
        self.cellDatasource = CellDatasource();
        cellDatasource!.delegate = self;
        cellDatasource!.setWidth(width, height: height)
        
        // Setup floating table view
        let colWidth = 120
        let rowHeight = 70
        
        var columnWidths = [Int]()
        for var i = 0; i < width; ++i {
            columnWidths.append(colWidth)
        }
        var rowHeights = [Int]()
        for var i = 0; i < height; ++i {
            rowHeights.append(rowHeight)
        }
        
        // Build the frames for the table border and the table
        let tableBorderFrame = CGRectMake(50.0, 200.0, self.view.frame.size.width - 100.0, self.view.frame.size.height - 275.0)
        let tableBorderSize = CGFloat(2.0)
        let tableFrame = CGRectMake(tableBorderSize, tableBorderSize, tableBorderFrame.size.width - tableBorderSize*2.0, tableBorderFrame.size.height - tableBorderSize*2.0)
        
        // Build the border view
        let tableBorder = UIView(frame: tableBorderFrame)
        tableBorder.backgroundColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1.0)
        
        // Build the table
        let tableParams = FloatingTableParameters(viewFrame : tableFrame,
            columnWidths : columnWidths,
            rowHeights : rowHeights,
            thresholdPercentage : 110,
            cellBufferX : 1.0,
            cellBufferY : 1.0)
        self.tableView = FloatingTableView(params: tableParams)
        
        let buttonWidth = 200.0 as CGFloat
        let buttonHeight = 50.0 as CGFloat
        
        // Build change color button
        let colorButtonFrame = CGRectMake(50.0, 75.0, buttonWidth, buttonHeight)
        let colorButton = UIButton()
        colorButton.frame = colorButtonFrame
        colorButton.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
        colorButton.setTitle("Change Color", forState: UIControlState.Normal)
        colorButton.setTitleColor(UIColor.redColor(), forState: UIControlState.Highlighted)
        colorButton.addTarget(tableView, action: "changeColor", forControlEvents: UIControlEvents.TouchUpInside)
        
        // Build request data button
        let dataButtonFrame = CGRectMake(colorButtonFrame.origin.x + buttonWidth*2.0, 75.0, buttonWidth, buttonHeight)
        let dataButton = UIButton()
        dataButton.frame = dataButtonFrame
        dataButton.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
        dataButton.setTitle("Request Cell Data", forState: UIControlState.Normal)
        dataButton.setTitleColor(UIColor.redColor(), forState: UIControlState.Highlighted)
        dataButton.addTarget(self, action: "requestCellData", forControlEvents: UIControlEvents.TouchUpInside)
        
        tableBorder.addSubview(tableView!)
        self.view.addSubview(tableBorder)
        self.view.addSubview(colorButton)
        self.view.addSubview(dataButton)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: CellDatasourceDelegate methods
    func requestCellData() {
        cellDatasource!.startConnection()
    }
    
    func cellResultsReturn(results: [[String]]) {
        tableView!.updateCellData(results)
    }
}

