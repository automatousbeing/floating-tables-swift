//
//  FloatingTableView.swift
//  FloatingTables-Swift
//
//  Created by Stephan Hennion on 7/31/15.
//  Copyright (c) 2015 Stephan Hennion. All rights reserved.
//

import Foundation
import UIKit

class FloatingTableView : UIScrollView, UIScrollViewDelegate {
    let state : FloatingTableState
    let params : FloatingTableParameters
    
    init (params : FloatingTableParameters) {
        self.params = params
        self.state = FloatingTableState(viewFrame: self.params.viewFrame, width: self.params.columnSum, height: self.params.rowSum, thresholdPercentage: self.params.thresholdPercentage)
        
        super.init(frame : params.viewFrame)
        
        self.delegate = self
        self.bounces = false
        
        // Initialize
        self.state.cellData = buildExampleCellArrayWithWidth(self.params.columnWidths.count, height: self.params.rowHeights.count)
        
        self.state.contentView = UIView(frame: CGRectMake(0.0, 0.0, CGFloat(params.columnSum), CGFloat(params.rowSum)))
        self.state.contentView.backgroundColor = UIColor(red: 211.0/255.0, green: 211.0/255.0, blue: 211.0/255.0, alpha: 1.0)
        
        self.addSubview(self.state.contentView)
        
        self.contentSize = CGSizeMake(CGFloat(params.columnSum), CGFloat(params.rowSum))
        
        self.state.viewBox = generateViewBoxFromOffset(self.contentOffset, thresholdRect: self.state.thresholdRect, thresholdPercentage: self.params.thresholdPercentage)
        self.state.contentBox = generateContentCellBoxFromViewCellBox(self.state.viewBox)
        
        let keysNeeded = keysNeededForContentBox(self.state.contentBox)
        setupCellsFromKeysNeeded(keysNeeded, keysNotNeeded: [String](), offsetIn: CGPointMake(0.0, 0.0))
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildExampleCellArrayWithWidth(width : Int, height : Int) -> [[String]] {
        var rows = [[String]]()
        for var i = 0; i < width; ++i {
            var column = [String]()
            for var j = 0; j < height; ++j {
                let data = String(format: "%d,%d", i,j)
                column.append(data)
            }
            rows.append(column)
        }
        
        return rows
    }
    
    // MARK: External Methods
    // Swaps colors between rainbow mode and normal white/grey alternating rows
    func changeColor() {
        if self.state.colorType == 1 {
            self.state.colorType = 0
        } else {
            self.state.colorType = 1
        }
        
        var cellColor : UIColor
        
        for (key,cell) in self.state.activeCells {
            let keyPoint = convertKeyToPoint(key)
            
            if self.state.colorType == 0 {
                if Int(keyPoint.y)%2 == 1 {
                    cellColor = UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1.0)
                } else {
                    cellColor = UIColor.whiteColor()
                }
            } else {
                cellColor = createColorWithIndex(Int(keyPoint.x), total: self.params.columnWidths.count, yValue: keyPoint.y)
            }
            
            cell.backgroundColor = cellColor
        }
    }
    
    func updateCellData(cellDataIn : [[String]]) {
        self.state.cellData = cellDataIn
        
        for (key,cell) in self.state.activeCells {
            let keyPoint = convertKeyToPoint(key)
            
            if cell.subviews.count > 0 {
                let cellLabel = cell.subviews[0] as! UILabel
                let row = self.state.cellData[Int(keyPoint.x)]
                let cellInfo = row[Int(keyPoint.y)]
                cellLabel.text = cellInfo
            }
        }
    }
    
    // MARK: Generation Methods
    // This is generating a box of form (Starting Column X, Starting Row Y, Number of Columns Wide, Number of Rows High)
    func generateViewBoxFromOffset(offsetIn : CGPoint, thresholdRect : CGRect, thresholdPercentage : CGFloat) -> CGRect {
        let workingOffset = generateAdjustedOffsetWithOffset(offsetIn, thresholdPercentage: thresholdPercentage, viewFrame: self.params.viewFrame)
        let offsetX = Int(workingOffset.x)
        let offsetY = Int(workingOffset.y)
        
        // The origin values are going to correspond to the starting index
        // The cutOff values are the precise point
        var boxOriginX = 0;
        var xCutOff = 0;
        
        var boxOriginY = 0;
        var yCutOff = 0;
        
        var boxWidth = 0;
        var boxHeight = 0;
        
        // columnTotal only used for calculations in this loop
        var columnTotal = 0;
        // This loop is calculating the X origin index, as well as the X cutoff point
        for var i = 0; i < self.params.columnWidths.count; ++i {
            let curSize = self.params.columnWidths[i]
            // Find relevant column
            if i == 0 {
                if offsetX < curSize {
                    boxOriginX = 0;
                    xCutOff = curSize - offsetX
                    break;
                }
            } else if i == (self.params.columnWidths.count-1) {
                if(offsetX < (columnTotal+curSize)) {
                    boxOriginX = i;
                    xCutOff = (columnTotal+curSize) - offsetX
                    break;
                }
            } else {
                if (offsetX >= columnTotal) && (offsetX <= (columnTotal + curSize)) {
                    boxOriginX = i
                    xCutOff = (columnTotal+curSize) - offsetX
                    break;
                }
            }
            columnTotal += curSize
        }
        
        // rowTotal only used for calculations in this loop
        var rowTotal = 0
        // This loop is calculating the Y origin index, as well as the Y cutoff point
        for var i = 0; i < self.params.rowHeights.count; ++i {
            let curSize = self.params.rowHeights[i]
            // Find relevant row
            if i == 0 {
                if offsetY < curSize {
                    boxOriginY = 0;
                    yCutOff = curSize - offsetY
                    break
                }
            } else if i == (self.params.rowHeights.count-1) {
                if offsetY < (rowTotal+curSize) {
                    boxOriginY = i
                    yCutOff = (rowTotal+curSize) - offsetY
                    break;
                }
            } else {
                if (offsetY >= rowTotal) && (offsetY <= (rowTotal + curSize)) {
                    boxOriginY = i
                    yCutOff = (rowTotal+curSize) - offsetY
                    break;
                }
            }
            rowTotal += curSize
        }
        
        // Width total is finding the total width of the column sizes combined such that we get enough
        // cells to be wider than the threshold box
        var widthTotal = xCutOff
        for var i = boxOriginX+1; i < self.params.columnWidths.count; ++i {
            let curSize = self.params.columnWidths[i]
            widthTotal += curSize
            
            if widthTotal > Int(thresholdRect.size.width) {
                boxWidth = (i - boxOriginX) + 1;
                break;
            }
        }
        // If the calculated width is 0, default to difference between the calculated starting 
        // point and the end of the columns
        if boxWidth == 0 {
            boxWidth = self.params.columnWidths.count - boxOriginX
        }
        
        // Height total is finding the total height of the row sizes combined such that we get enough
        // cells to be taller than the threshold box
        var heightTotal = yCutOff
        for var i = boxOriginY+1; i < self.params.rowHeights.count; ++i {
            let curSize = self.params.rowHeights[i]
            heightTotal += curSize
            
            if heightTotal > Int(thresholdRect.size.height) {
                boxHeight = (i - boxOriginY)+1;
                break;
            }
        }
        // If the calculated height is 0, default to the difference between the calculated starting
        // point and the bottom of the rows
        if boxHeight == 0 {
            boxHeight = self.params.rowHeights.count - boxOriginY
        }
        
        return CGRectMake(CGFloat(boxOriginX), CGFloat(boxOriginY), CGFloat(boxWidth), CGFloat(boxHeight))
    }
    
    func generateContentCellBoxFromViewCellBox(viewBox : CGRect) -> CGRect {
        var boxOriginX = 0;
        var boxOriginY = 0;
        var boxWidth = 0;
        var boxHeight = 0;
        
        // If the view box's origin is at the view origin, then the content cell must be as well
        if viewBox.origin.x == 0 {
            boxOriginX = 0
        } else {
            boxOriginX = Int(viewBox.origin.x) - 1
        }
        
        if viewBox.origin.y == 0 {
            boxOriginY = 0;
        } else {
            boxOriginY = Int(viewBox.origin.y) - 1
        }
        
        // We're adding 2 onto viewCellRectIn's width in order to accomodate a 'cell padding' of size 1 on either side of
        // the view box
        boxWidth = Int(viewBox.size.width) + 2;
        
        // If this results in a box that goes further out than our total columns, we adjust the origin in order to keep the 
        // content box within bounds
        if (boxOriginX + boxWidth) > self.params.columnWidths.count {
            // Check to see if the origin is going to be pushed into the negatives (bad) and adjust accordingly
            if (self.params.columnWidths.count - boxWidth) < 0 {
                let originDiff = boxOriginX
                boxOriginX = 0
                boxWidth += originDiff
                
                if boxWidth > self.params.columnWidths.count {
                    boxWidth = self.params.columnWidths.count
                }
            } else {
                boxOriginX = self.params.columnWidths.count - boxWidth
            }
        }
        
        // Likewise for the height
        boxHeight = Int(viewBox.size.height) + 2;
        
        if (boxOriginY + boxHeight) > self.params.rowHeights.count {
            if (self.params.rowHeights.count - boxHeight) < 0 {
                let originDiff = boxOriginY
                boxOriginY = 0
                boxHeight += originDiff
                
                if boxHeight > self.params.rowHeights.count {
                    boxHeight = self.params.rowHeights.count
                }
            } else {
                boxOriginY = self.params.rowHeights.count - boxHeight
            }
        }
        
        return CGRectMake(CGFloat(boxOriginX), CGFloat(boxOriginY), CGFloat(boxWidth), CGFloat(boxHeight));
    }
    
    // Returns offset adjusted by the distance between the view rectangle and the threshold rectangle
    func generateAdjustedOffsetWithOffset(offset : CGPoint, thresholdPercentage : CGFloat, viewFrame : CGRect) -> CGPoint {
        let newOffsetX = offset.x + ((viewFrame.size.width - viewFrame.size.width*(thresholdPercentage/100.0))/2.0);
        let newOffsetY = offset.y + ((viewFrame.size.height - viewFrame.size.height*(thresholdPercentage/100.0))/2.0);
        
        return CGPointMake(newOffsetX, newOffsetY)
    }
    
    // MARK: Key Methods
    // This generates an array of keys which represent the (x,y) pairs needed for the contentBox passed in
    func keysNeededForContentBox(contentBoxIn : CGRect) -> [String] {
        var keysNeeded = [String]()
        let boxX = Int(contentBoxIn.origin.x)
        let boxY = Int(contentBoxIn.origin.y)
        let boxWidth = Int(contentBoxIn.size.width)
        let boxHeight = Int(contentBoxIn.size.height)
        
        for var i = 0; i < boxWidth; ++i {
            for var j = 0; j < boxHeight; ++j {
                let keyX = boxX + i;
                let keyY = boxY + j;
                
                let key = NSString(format: "%d,%d", keyX, keyY)
                keysNeeded.append(key as String)
            }
        }
        
        return keysNeeded
    }

    // MARK: Box Methods
    func equalityBetweenNewContentBox(newBox : CGRect, oldBox: CGRect) -> Bool {
        if newBox.origin.x != oldBox.origin.x {
            return false;
        }
        if newBox.origin.y != oldBox.origin.y {
            return false;
        }
        if newBox.size.width != oldBox.size.width {
            return false;
        }
        if newBox.size.height != oldBox.size.height {
            return false;
        }
        
        return true
    }
    
    // MARK: Cell Adjustment Methods
    // This releases all the cells form the keysNotNeeded array into the cellPool, and then checks if the cellpool has enough cells to
    // build a cell for everything in the keysNeeded array.  Finally, adjustCells is called with the keysNeeded array
    func setupCellsFromKeysNeeded(keysNeeded : [String], keysNotNeeded : [String], offsetIn : CGPoint) {
        for key in keysNotNeeded {
            let cell = self.state.activeCells[key]
            self.state.activeCells.removeValueForKey(key)
            cell?.hidden = true
            self.state.cellPool.append(cell!)
        }
        
        if keysNeeded.count > self.state.cellPool.count {
            generateCellsWithAmount(keysNeeded.count - self.state.cellPool.count)
        }
        
        adjustCellsWithOffset(offsetIn, stringKeys: keysNeeded, pointKeys: [])
    }
    
    // This determines whether cells need to be generated for the cellPool before adjustments are made to cells following a scroll cell
    // adjustment
    func setupCellsFromPointsNeeded(keysNeeded : [CGPoint], offsetIn : CGPoint) {
        if keysNeeded.count > self.state.cellPool.count {
            generateCellsWithAmount(keysNeeded.count - self.state.cellPool.count)
        }
        
        adjustCellsWithOffset(offsetIn, stringKeys: [], pointKeys: keysNeeded)
    }
    
    // This takes an array of keys to be made active, and sets up their corresponding cells
    func adjustCellsWithOffset(offsetIn : CGPoint, stringKeys : [String], pointKeys : [CGPoint]) {
        let bufferOffsetX = self.params.cellBufferX/2.0 + 0.5 + offsetIn.x
        let bufferOffsetY = self.params.cellBufferY/2.0 + offsetIn.y
        
        if stringKeys.count != 0 {
            for key in stringKeys {
                let keyPoint = convertKeyToPoint(key)
                adjustSingleCellWithKeyPoint(keyPoint, bufferOffsetX: bufferOffsetX, bufferOffsetY: bufferOffsetY)
            }
        } else if pointKeys.count != 0 {
            for keyPoint in pointKeys {
                adjustSingleCellWithKeyPoint(keyPoint, bufferOffsetX: bufferOffsetX, bufferOffsetY: bufferOffsetY)
            }
        }
    }
    
    // In order to handle a single function that can adjust the cells with String keys or CGPoint keys, the majority
    // of the logic for adjusting cells was pulled out and put into this function
    func adjustSingleCellWithKeyPoint(keyPoint : CGPoint, bufferOffsetX : CGFloat, bufferOffsetY : CGFloat) {
        // Grab cell from the cell pool and adjust its size and position
        let cell = self.state.cellPool.last
        if cell?.subviews.count > 0 {
            cell?.subviews[0].removeFromSuperview()
        }
        self.state.cellPool.removeLast()
        let newFrameX = CGFloat(self.params.columnTotals[Int(keyPoint.x)]) + bufferOffsetX
        let newFrameY = CGFloat(self.params.rowTotals[Int(keyPoint.y)]) + bufferOffsetY
        let newFrameWidth = CGFloat(self.params.columnWidths[Int(keyPoint.x)]) - self.params.cellBufferX
        let newFrameHeight = CGFloat(self.params.rowHeights[Int(keyPoint.y)]) - self.params.cellBufferY
        let newFrame = CGRectMake(newFrameX, newFrameY, newFrameWidth, newFrameHeight)
        cell?.frame = newFrame
        
        // Setup the cell's label from the attached cellData and attach it to the cell
        let cellLabel = UILabel(frame: CGRectMake(10.0, 0.0, newFrame.size.width, newFrame.size.height))
        cellLabel.backgroundColor = UIColor.clearColor()
        let row = self.state.cellData[Int(keyPoint.x)]
        let cellInfo = row[Int(keyPoint.y)]
        cellLabel.text = cellInfo
        cellLabel.numberOfLines = 0
        cellLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
        
        cell?.addSubview(cellLabel)
        
        var cellColor : UIColor
        
        if self.state.colorType == 0 {
            if Int(keyPoint.y)%2 == 1 {
                cellColor = UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1.0)
            } else {
                cellColor = UIColor.whiteColor()
            }
        } else {
            cellColor = createColorWithIndex(Int(keyPoint.x), total: self.params.columnWidths.count, yValue: keyPoint.y)
        }
        
        cell?.backgroundColor = cellColor
        
        cell?.hidden = false
        
        // Once placed into position and styled properly, add the cell to the activeCells dictionary using it's position
        // key as the dictionary key
        let key = NSString(format: "%d,%d", Int(keyPoint.x), Int(keyPoint.y))
        self.state.activeCells.updateValue(cell!, forKey: key as String)
    }

    func convertKeyToPoint(keyString : String) -> CGPoint {
        let range = (keyString as NSString).rangeOfString(",")
        //let commaIndex = range?.endIndex
        let xString = (keyString as NSString).substringToIndex(range.location)
        let yString = (keyString as NSString).substringFromIndex(range.location+1)
        let xVal = xString.toInt()
        let yVal = yString.toInt()
        return CGPointMake(CGFloat(xVal!), CGFloat(yVal!))
    }
    
    func generateCellsWithAmount(amount : Int) {
        for var i = 0; i < amount; ++i {
            let newCell = UIView(frame: CGRectMake(0.0, 0.0, 0.0, 0.0))
            newCell.backgroundColor = UIColor.blueColor()
            self.state.contentView.addSubview(newCell)
            self.state.cellPool.append(newCell)
        }
    }
    
    // MARK: Scroll Delegate
    func scrollViewDidScroll(scrollView: UIScrollView) {
        // Tell the header delegate that we've scrolled
        // This would be necessary for the header seen in the youTube video of the production version
        //[headerDelegate tableScrolled:self.contentOffset];
        
        let currentOffset = self.contentOffset
        
        var checkViewCell = false
        
        // checkoffset will be set equal to the curren offset further down, and this just sees whether we've scrolled enough to 
        // warrant a cell check, by looking at the smallest column and row sizes that would effect that decision
        if Int(abs((self.state.checkOffset.x - currentOffset.x))) > self.params.smallestColumn {
            checkViewCell = true
        }
        if Int(abs((self.state.checkOffset.y - currentOffset.y))) > self.params.smallestRow {
            checkViewCell = true
        }
        
        if (checkViewCell) {
            // Generate the new potential view and content boxes from the current offset
            let newViewBox = generateViewBoxFromOffset(currentOffset, thresholdRect: self.state.thresholdRect, thresholdPercentage: self.params.thresholdPercentage)
            let newContentBox = generateContentCellBoxFromViewCellBox(newViewBox)
            
            // Compare the current content box and the new theoretical to see if a cell adjustment is necessary
            let needToUpdateCells = !equalityBetweenNewContentBox(newContentBox, oldBox: self.state.contentBox)
            
            // This checks to make sure the contentBox actually has usable size to it
            var contentSum = Int(self.state.contentBox.size.width) + Int(self.state.contentBox.size.height)
            let widthCheck = Int(self.state.contentBox.size.width) == 0
            let heightCheck = Int(self.state.contentBox.size.height) == 0
            if (widthCheck || heightCheck) {
                contentSum = 0
            }
            
            // If the content box has size to it, and an update is necessary, we continue on
            if needToUpdateCells && (contentSum != 0) {
                self.state.checkOffset = self.contentOffset
                
                var keysNeeded = [CGPoint]()
                var keysNotNeeded = [String]()
                
                // These keep track of the rows/columns gained and lost from the scroll
                var xLost = [Int]()
                var xAdd = [Int]()
                var yLost = [Int]()
                var yAdd = [Int]()
                
                /* This whole monstrosity seems absurd, but it's actually easy to follow
                 * and runs fairly quickly, as the majority of this code is never hit.
                 * I know there's an arithmetic trick for determining the values I need, 
                 * but it wasn't immediately apparent, so I went with the tried and true
                 * conditional chain
                 */
                
                let newBeginX = Int(newContentBox.origin.x)
                let newWidthX = Int(newContentBox.size.width)
                let newEndX = newBeginX + newWidthX
                
                let oldBeginX = Int(self.state.contentBox.origin.x)
                let oldWidthX = Int(self.state.contentBox.width)
                let oldEndX = oldBeginX + oldWidthX
                
                // Check if scroll has moved forward in the x direction
                if newBeginX > oldBeginX {
                    // Check if scroll has generated content box extending further than previous
                    if newEndX > oldEndX {
                        // Check if scroll has placed content box beginning beyond old box end
                        if newBeginX > oldEndX {
                            // Find the total lost and gained, then record the associated indices
                            // This is different from the else on this conditional, as the entirety
                            // of the two content boxes must be recorded
                            let lostDiff = oldEndX - oldBeginX;
                            for var i = 0; i <= lostDiff; ++i {
                                xLost.append(oldBeginX+i)
                            }
                            let addDiff = newEndX - newBeginX;
                            for var i = 0; i <= addDiff; ++i {
                                xAdd.append(newBeginX+i)
                            }
                        } else {
                            // Scroll causes new content box to still overlap old in x direction
                            // This is only finding the appropriate differences between old and new
                            let lostDiff = newBeginX - oldBeginX
                            for var i = 0; i < lostDiff; ++i {
                                xLost.append(oldBeginX+i)
                            }
                            let addDiff = newEndX - oldEndX
                            for var i = 0; i < addDiff; ++i {
                                xAdd.append(oldEndX+i)
                            }
                        }
                    } else if newEndX == oldEndX {
                        // Scroll has placed the ends of the two boxes on top of each other, so
                        // only the amounts lost at the beginning of the boxes is recorded
                        let lostDiff = newBeginX - oldBeginX
                        for var i = 0; i < lostDiff; ++i {
                            xLost.append(oldBeginX+i)
                        }
                    } else if newEndX < oldEndX {
                        // Scroll has placed the new box entirely within the old box, without any
                        // equality on either side in the X direction, and therefore amounts lost
                        // on either side must be recorded
                        let lostDiffFront = newBeginX - oldBeginX
                        for var i = 0; i < lostDiffFront; ++i {
                            xLost.append(oldBeginX+i)
                        }
                        let lostDiffBack = oldEndX - newEndX
                        for var i = 0; i < lostDiffBack; ++i {
                            xLost.append(oldEndX-i)
                        }
                    }
                } else if newBeginX == oldBeginX {
                    // Scroll has overlapped the beginning of old and new content boxes
                    // Check which end extends further, and calculated loss/gains
                    // appropriately
                    if newEndX > oldEndX {
                        let addDiff = newEndX - oldEndX
                        for var i = 0; i < addDiff; ++i {
                            xAdd.append(oldEndX+i)
                        }
                    } else if newEndX < oldEndX {
                        let lostDiff = oldEndX - newEndX
                        for var i = 0; i < lostDiff; ++i {
                            xLost.append(oldEndX - 1 - i)
                        }
                    }
                } else if newBeginX < oldBeginX {
                    // Scroll has moved the content box backwards in the X direction
                    // Check to see where the ends of the old and new content boxes fall
                    // and calculate loss/gains appropriately
                    if oldEndX > newEndX {
                        if newEndX < oldBeginX {
                            let lostDiff = oldEndX - oldBeginX
                            for var i = 0; i < lostDiff; ++i {
                                xLost.append(oldBeginX+i)
                            }
                            let addDiff = newEndX - newBeginX
                            for var i = 0; i < addDiff; ++i {
                                xAdd.append(newBeginX+i)
                            }
                        } else {
                            let lostDiff = oldEndX - newEndX
                            for var i = 0; i < lostDiff; ++i {
                                xLost.append(oldEndX - 1 - i)
                            }
                            let addDiff = oldBeginX - newBeginX
                            for var i = 0; i < addDiff; ++i {
                                xAdd.append(newBeginX+i)
                            }
                        }
                    } else if oldEndX == newEndX {
                        let addDiff = oldBeginX - newBeginX
                        for var i = 0; i < addDiff; ++i {
                            xAdd.append(newBeginX+i)
                        }
                    } else if oldEndX < newEndX {
                        let addDiff = oldBeginX - newBeginX
                        for var i = 0; i < addDiff; ++i {
                            xAdd.append(newBeginX+i)
                        }
                        let lostDiff = newEndX - oldEndX
                        for var i = 0; i < lostDiff; ++i {
                            xLost.append(newEndX-i)
                        }
                    }
                }
                
                // Once the gains and losses in the X direction have been found, these two loops generate the
                // keys from the recorded losses and gains, and add them keysNeeded and keysNotNeeded arrays
                // respectively
                for xVal in xLost {
                    let contentBoxOriginY = Int(self.state.contentBox.origin.y)
                    for var i = 0; i < Int(self.state.contentBox.size.height); ++i {
                        let keyString = String(format: "%d,%d", xVal,contentBoxOriginY+i)
                        keysNotNeeded.append(keyString)
                    }
                }
                for xVal in xAdd {
                    let contentBoxOriginY = newContentBox.origin.y
                    for var i = 0; i < Int(newContentBox.size.height); ++i {
                        let keyPoint = CGPointMake(CGFloat(xVal), contentBoxOriginY+CGFloat(i))
                        keysNeeded.append(keyPoint)
                    }
                }
                
                // The following section repeats the process for the Y (height) axis
                let newBeginY = Int(newContentBox.origin.y)
                let newHeightY = Int(newContentBox.size.height)
                let newEndY = newBeginY + newHeightY
                
                let oldBeginY = Int(self.state.contentBox.origin.y)
                let oldHeightY = Int(self.state.contentBox.size.height)
                let oldEndY = oldBeginY + oldHeightY
                
                if newBeginY > oldBeginY {
                    if newEndY > oldEndY {
                        if newBeginY > oldEndY {
                            let lostDiff = oldEndY - oldBeginY
                            for var i = 0; i <= lostDiff; ++i {
                                yLost.append(oldBeginY+i)
                            }
                            let addDiff = newEndY - newBeginY
                            for var i = 0; i <= addDiff; ++i {
                                yAdd.append(newBeginY+i)
                            }
                        } else {
                            let lostDiff = newBeginY - oldBeginY
                            for var i = 0; i < lostDiff; ++i {
                                yLost.append(oldBeginY+i)
                            }
                            let addDiff = newEndY - oldEndY
                            for var i = 0; i < addDiff; ++i {
                                yAdd.append(oldEndY+i)
                            }
                        }
                    } else if newEndY == oldEndY {
                        let lostDiff = newBeginY - oldBeginY
                        for var i = 0; i < lostDiff; ++i {
                            yLost.append(oldBeginY+i)
                        }
                    } else if newEndY < oldEndY {
                        let lostDiffFront = newBeginY - oldBeginY
                        for var i = 0; i < lostDiffFront; ++i {
                            yLost.append(oldBeginY+i)
                        }
                        let lostDiffBack = oldEndY - newEndY
                        for var i = 0; i < lostDiffBack; ++i {
                            yLost.append(oldEndY-i)
                        }
                    }
                } else if newBeginY == oldBeginY {
                    if newEndY > oldEndY {
                        let addDiff = newEndY - oldEndY
                        for var i = 0; i < addDiff; ++i {
                            yAdd.append(oldEndY+i)
                        }
                    } else if newEndY < oldEndY {
                        let lostDiff = oldEndY - newEndY
                        for var i = 0; i < lostDiff; ++i {
                            yLost.append(oldEndY-1-i)
                        }
                    }
                } else if newBeginY < oldBeginY {
                    if oldEndY > newEndY {
                        if newEndY < oldBeginY {
                            let lostDiff = oldEndY - oldBeginY
                            for var i = 0; i < lostDiff; ++i {
                                yLost.append(oldBeginY+i)
                            }
                            let addDiff = newEndY - newBeginY
                            for var i = 0; i < addDiff; ++i {
                                yAdd.append(newBeginY+i)
                            }
                        } else {
                            let lostDiff = oldEndY - newEndY
                            for var i = 0; i < lostDiff; ++i {
                                yLost.append(oldEndY-1-i)
                            }
                            let addDiff = oldBeginY - newBeginY
                            for var i = 0; i < addDiff; ++i {
                                yAdd.append(newBeginY+i)
                            }
                        }
                    } else if oldEndY == newEndY {
                        let addDiff = oldBeginY - newBeginY
                        for var i = 0; i < addDiff; ++i {
                            yAdd.append(newBeginY+i)
                        }
                    } else if oldEndY < newEndY {
                        let addDiff = oldBeginY - newBeginY
                        for var i = 0; i < addDiff; ++i {
                            yAdd.append(newBeginY+i)
                        }
                        let lostDiff = newEndY - oldEndY
                        for var i = 0; i < lostDiff; ++i {
                            yLost.append(newEndY-i)
                        }
                    }
                }
                
                
                // These do the same thing as the corresponding loops in the X direction, but also check that they
                // aren't entering duplicates into the arrays
                for yVal in yLost {
                    let contentBoxOriginX = Int(self.state.contentBox.origin.x)
                    for var i = 0; i < Int(self.state.contentBox.size.width); ++i {
                        let keyString = String(format: "%d,%d", contentBoxOriginX+i, yVal)
                        if !contains(keysNotNeeded, keyString) {
                            keysNotNeeded.append(keyString)
                        }
                    }
                }
                for yVal in yAdd {
                    let contentBoxOriginX = newContentBox.origin.x
                    for var i = 0; i < Int(newContentBox.size.width); ++i {
                        let keyPoint = CGPointMake(contentBoxOriginX + CGFloat(i), CGFloat(yVal))
                        if !contains(keysNeeded, keyPoint) {
                            keysNeeded.append(keyPoint)
                        }
                    }
                }
                
                // This releases keysNotNeeded into the cellPool
                for oldKey in keysNotNeeded {
                    let cell = self.state.activeCells[oldKey]
                    self.state.activeCells.removeValueForKey(oldKey)
                    cell?.hidden = true
                    self.state.cellPool.append(cell!)
                }
                
                // This gets an empty offset passed in, as from what I can tell in the obj-c version, the value that was
                // passed in was never initialized
                setupCellsFromPointsNeeded(keysNeeded, offsetIn:CGPointMake(0.0, 0.0))
                
                self.state.viewBox = newViewBox
                self.state.contentBox = newContentBox
            }
        }
    }
    
    // MARK: Misc Methods
    // These two methods generate the color at a specific point for a sort of "color spectrum box", which attempts to 
    // draw a rainbow left to right, and a saturation range top to bottom.
    // The heart of it lies in the three color arrays.  The represent the position of the RGB values along a line graph 
    // which is displaying the "color spectrum".  The x is used against the total width in order to determine where along
    // that color line the current cell falls.  The y value is just used in the second method for some trickery that 
    // uses a formula taking brightness and saturation into consideration when modifying RGB values.
    func createColorWithIndex(index : Int, total : Int, yValue : CGFloat) -> UIColor {
        var newColor : UIColor
        
        let redArray = [1,2,0,0,3,1]
        let greenArray = [0,0,3,1,1,2]
        let blueArray = [3,1,1,2,0,0]
        
        let inputPositionTrue = (CGFloat(index)*6.0)/CGFloat(total)
        let inputPosition = Int(floor(inputPositionTrue))
        let redType = redArray[inputPosition]
        let greenType = greenArray[inputPosition]
        let blueType = blueArray[inputPosition]
        
        let yPercent = yValue/CGFloat(total)
        
        let redVal = colorCalculationType(redType, positionTrue: CGFloat(inputPositionTrue), positionInt: inputPosition, yPercent: yPercent)
        let greenVal = colorCalculationType(greenType, positionTrue: CGFloat(inputPositionTrue), positionInt: inputPosition, yPercent: yPercent)
        let blueVal = colorCalculationType(blueType, positionTrue: CGFloat(inputPositionTrue), positionInt: inputPosition, yPercent: yPercent)
        
        newColor = UIColor(red: redVal, green: greenVal, blue: blueVal, alpha: 1.0)
        
        return newColor
    }
    
    func colorCalculationType(type : Int, positionTrue: CGFloat, positionInt: Int, yPercent: CGFloat) -> CGFloat {
        var returnValue = 0.0 as CGFloat
        let indexPercentage = positionTrue - CGFloat(positionInt)
        
        let brightness = 1.0 as CGFloat
        let saturation = 1.0 * yPercent
        
        let minVal = (1.0 - saturation) * brightness
        let maxVal = 1.0 - (1.0 - brightness)
        let range = maxVal - minVal
        
        switch type {
        case 0:
            returnValue = minVal
            break
        case 1:
            returnValue = maxVal
            break
        case 2:
            returnValue = maxVal - (range * indexPercentage)
            break
        case 3:
            returnValue = minVal + (range * indexPercentage)
            break
        default:
            break
        }
        
        return returnValue
    }
}