//
//  FloatingTableState.swift
//  FloatingTables-Swift
//
//  Created by Stephan Hennion on 8/2/15.
//  Copyright (c) 2015 Stephan Hennion. All rights reserved.
//

import Foundation
import UIKit

// This class represents the objects that will change during execution for FloatingTableView
class FloatingTableState {
    var cellPool : [UIView]
    var cellData : [[String]]
    var activeCells : [String:UIView]
    var contentView : UIView
    var thresholdRect : CGRect
    var viewBox : CGRect
    var contentBox : CGRect
    var checkOffset : CGPoint
    var colorType : Int
    
    init(viewFrame : CGRect, width : Int, height : Int, thresholdPercentage : CGFloat) {
        self.cellPool = []
        self.cellData = []
        self.activeCells = [:]
        contentView = UIView(frame: CGRectMake(0.0, 0.0, CGFloat(width), CGFloat(height)))
        self.thresholdRect = CGRectMake(0.0, 0.0, 0.0, 0.0)
        self.viewBox = CGRectMake(0.0, 0.0, 0.0, 0.0)
        self.contentBox = CGRectMake(0.0, 0.0, 0.0, 0.0)
        self.checkOffset = CGPointMake(0.0, 0.0)
        self.colorType = 0
        self.thresholdRect = generateThresholdRectWithWidth(viewFrame.size.width, height: viewFrame.size.height, thresholdPercentage: thresholdPercentage)
    }
}

extension FloatingTableState {
    // Build a rect centered within the associated view frame, but smaller by the given threshold percentage
    func generateThresholdRectWithWidth(width : CGFloat, height : CGFloat, thresholdPercentage : CGFloat) -> CGRect {
        let originX = width/2.0 - ((width*(thresholdPercentage/100.0))/2.0);
        let originY = height/2.0 - ((height*(thresholdPercentage/100.0))/2.0);
        let rectWidth = width*(thresholdPercentage/100.0);
        let rectHeight = height*(thresholdPercentage/100.0);
        return CGRectMake(originX, originY, rectWidth, rectHeight);
    }
}